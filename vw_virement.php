<?php
  require_once('include.php');
  
  session_start();
  
  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {
      // utilisateur non connecté
      header('Location: vw_login.php');      
      exit();
  }
  
  $mytoken = bin2hex(random_bytes(128)); // token qui va servir à prévenir des attaques CSRF 
  $_SESSION["mytoken"] = $mytoken;
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Mon Compte</title>
  <link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
</head>
<body>
    <header>
        <form method="POST" action="myController.php">
            <button class="btn-back form-btn">Retour</button>
        </form>
        <form method="POST" action="myController.php">
            <input type="hidden" name="action" value="disconnect">
            <button class="btn-logout form-btn">Déconnexion</button>
        </form>
        
        <h2><?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?> - Mon compte</h2>
    </header>
    
    <section>
        
        <article>
        <form method="POST" action="myController.php">
          <input type="hidden" name="action" value="transfert">
          <input type="hidden" name="mytoken" value="<?php echo $mytoken; ?>">
          <div class="fieldset">
              <div class="fieldset_label">
                  <span>Transférer de l'argent</span>
              </div>
              <div class="field">
              <label>N° compte destinataire : </label><input type="text" value="<?= $_GET['compte']?>"size="20" name="destination">
              </div>
              <div class="field">
                  <label>Montant à transférer : </label><input type="text" size="10" name="montant">
              </div>
              <button class="form-btn">Transférer</button>
              <?php
              if (isset($_REQUEST["err_token"])) {
                echo '<p>Echec virement : le contrôle d\'intégrité a échoué.</p>';
              }
              if (isset($_REQUEST["trf_ok"])) {
                echo '<p>Virement effectué avec succès.</p>';
              }
              if (isset($_REQUEST["bad_mt"])) {
                echo '<p>Le montant saisi est incorrect : '.htmlentities($_REQUEST["bad_mt"], ENT_QUOTES).'</p>';
              }
              ?>
          </div>
        </form>
        </article>
        
    </section>

</body>
</html>
