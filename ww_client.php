<?php
  require_once('include.php');
  require_once('myModel.php');

  session_start();
  
  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {
      // utilisateur non connecté
      header('Location: vw_login.php');      
      exit();
  }
  
  if($_SESSION["connected_user"]["profil_user"] != "EMPLOYE"){
      header('Location: vw_moncompte.php');      
      exit();
  }

  $user_display = "";
  if($_GET["user"]){
      $liste_login = array();
      foreach($_SESSION['listeUsers'] as $user){
          array_push($liste_login, $user['login']);
      }
      if(in_array($_GET["user"], $liste_login)){
          $user_display = $_GET["user"];
          foreach($_SESSION['listeUsers'] as $user){
              if($user['login'] == $_GET['user']){
                  $user_info = $user;
              }
              $user_solde_info = getSoldeInfo($_GET['user']);  
          }
      }
  }

  $url = $_SERVER['PHP_SELF'];

  $mytoken = bin2hex(random_bytes(128)); // token qui va servir à prévenir des attaques CSRF 
  $_SESSION["mytoken"] = $mytoken;
?>


<!doctype html> 

<html lang="fr">
 <head>
  <meta charset="utf-8">
  <title>Fiche client</title>
  <link rel="stylesheet" type="text/css" media="all"  href="css/mystyle.css" />
  <link rel="stylesheet" type="text/css" media="all"  href="css/client.css" />
</head>
<body>
    <header>
        <form method="POST" action="myController.php">
            <button class="btn-back form-btn">Retour</button>
        </form>
        <form method="POST" action="myController.php">
            <input type="hidden" name="action" value="disconnect">
            <button class="btn-logout form-btn">Déconnexion</button>
        </form>
        
        <h2><?= $_SESSION["connected_user"]["prenom"];?> <?= $_SESSION["connected_user"]["nom"];?> - Fiche client</h2>
    </header>
    
    <section id="flex">
		<div>
            <ul>
		        <?php foreach($_SESSION["listeUsers"] as $id => $user){ ?>
                    <li><a href="<?= $url . '?user=' . $user['login'] ?>"><?= $user["nom"] . " " . $user["prenom"] ?></a></li>
		        <?php } ?>
		    </ul>
		</div>	


    <div>
        <?php if($user_display != ""){ ?>
        <h2>Login:<?= $user_info['login'] ?></h2>
       <p>Nom:<?= $user_info['nom'] ?></p>
       <p>Prenom:<?= $user_info['prenom'] ?></p>
       <p>Profile:<?= $user_info['profil_user'] ?></p> 
       <p>Numéro de compte:<?= $user_solde_info['numero_compte']?></p>
       <p>Solde: <?= $user_solde_info['solde_compte']?></p>
       <a href="vw_virement.php?compte=<?= $user_solde_info['numero_compte']?>">Virement</a>
       <?php } ?>
    </div>
        
    </section>

</body>
</html>
